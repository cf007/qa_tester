# QA Test Engineer Assignment #

Please complete the following assignment. Upload your final answer to your github account. If you have set up this as a heroku project, please provide the link.

### Questions ###

1. What is your favorite test framework and why?

2. Given the following sample_account.py (see source code in repo), please write a unit test suite for the class.

3. Set up your unit test suite on github and travis CI.


